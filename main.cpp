#include <iostream>
#include "pessoa.hpp"
#include "pessoa.cpp"

using namespace std;

//main file
int main(){
	Pessoa umaPessoa;
	
	cout << "Nome: " << umaPessoa.getNome() << endl;
	cout << "Idade: " << umaPessoa.getIdade() << endl;
	
	umaPessoa.setNome("Paulo");
	umaPessoa.setIdade("32");
	
	cout << "Nome: " << umaPessoa.getNome() << endl;
	cout << "Idade: " <<umaPessoa.getIdade() << endl;
	
}
